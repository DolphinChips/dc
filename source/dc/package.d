module dc;

import std.algorithm.comparison : predSwitch;
import std.conv : to;
import std.string : strip;

/// https://dlang.org/blog/2017/02/13/a-new-import-idiom/
template from(alias name) {
	mixin("import from = " ~ name ~ ";");
}

unittest {
	static assert(__traits(compiles, from!"std".writeln()));
	static assert(__traits(compiles, from!"std.stdio".readln()));
}

/// Enum that contains compile date in YYYYMMDD format
enum compileDate =
	__DATE__[7 .. $].to!int * 10000
	+ __DATE__[0 .. 3].predSwitch(
		"Jan", 1,
		"Feb", 2,
		"Mar", 3,
		"Apr", 4,
		"May", 5,
		"Jun", 6,
		"Jul", 7,
		"Aug", 8,
		"Sep", 9,
		"Oct", 10,
		"Nov", 11,
		"Dec", 12
	) * 100
	+ __DATE__[4 .. 6].strip.to!int;

unittest {
	import std.math : log10;
	static assert(cast(int) log10(compileDate) == 7, "Date \"" ~ compileDate.to!string ~ "\" isn't 8 numbers long");
	enum month = compileDate / 100 % 100;
	static assert(month >= 1 && month <= 12, "Invalid month: " ~ month.to!string);
	enum day = compileDate % 100;
	static assert(day >= 1 && day <= 31, "Invalid day: " ~ day.to!string);
}

/// Template that calls `static assert` if compile date is past of what was passed as an argument. Go figure out what to do with that.
template bestBefore(int deprecatedStamp) {
	static assert(compileDate < deprecatedStamp);
}

unittest {
	static assert(__traits(compiles, {
		@bestBefore!99991231
		struct A { }
	}));
	static assert(!__traits(compiles, {
		@bestBefore!19700101
		struct A { }
	}));
}

/++
$(P Struct that provides following things:)
1. RAII wrapper above handle type
2. Mechanism that strips prefix from names of functions in given module. Those functions are available through `opDispatch`-based mechanism.

Params:
	module_		= module with functions from which given prefix should be stripped
	prefix		= prefix that should be stripped from names of functions
	constructor	= a function that creates a handle. Note: constructor must take at least one non-optional argument
	destructor	= function that gets called when struct gets destroyed. Mustn't take any arguments

Examples:
---
import etc.c.curl;
alias Curl = CWrap!(etc.c.curl, "curl_easy_", (int _) => curl_easy_init(), curl_easy_cleanup);
auto c = Curl(0);
c.setopt(CurlOption.url, "https://dlang.org");
c.perform();
---
+/
struct CWrap(alias module_, string prefix, alias constructor, alias destructor) {
	import std.traits : Parameters, ReturnType;

	ReturnType!constructor handle;
	alias handle this;

	@disable this();
	@disable this(ref typeof(this));
	this(Parameters!constructor args) {
		handle = constructor(args);
	}

	~this() {
		destructor(handle);
	}

	template opDispatch(string f) {
		alias fun = __traits(getMember, module_, prefix ~ f);
		alias returnType = ReturnType!fun;
		returnType opDispatch(Args...)(Args args) {
			static if(is(returnType == void)) {
				fun(handle, args);
			} else {
				return fun(handle, args);
			}
		}
	}
}

unittest {
	bool didDestroy = false;
	struct Foo_ {
		int a;
	}
	Foo_* foo_new(int a) {
		return new Foo_(a);
	}
	void foo_del(Foo_* h) @trusted {
		h.a = 0;
		from!"core.memory".GC.free(h);
		didDestroy = true;
	}
	int foo_get_a(Foo_* h) {
		return h.a;
	}
	void foo_set_a(Foo_* h, int a) {
		h.a = a;
	}
	struct mod {
		alias foo__new = foo_new;
		alias foo__del = foo_del;
		alias foo__get_a = foo_get_a;
		alias foo__set_a = foo_set_a;
	}
	alias Foo = CWrap!(mod, "foo__", foo_new, foo_del);
	{
		auto foo = Foo(3);
		assert(foo.get_a == 3);
		foo.set_a(4);
		assert(foo.get_a == 4);
	}
	assert(didDestroy);
}

/++
A copy of std::tie from C++.
TODO: write proper documentation for this
Examples:
---
int a;
string b;
unwrap(a, b) = tuple(3, "hi");
assert(a == 3 && b == hi);
---
+/
auto unwrap(T...)(out T args) {
	struct UnwrapImpl {
		void opAssign(U)(U tuple) if(U.length == T.length) {
			static foreach(i; 0 .. U.length) {
				args[i] = tuple[i];
			}
		}
	}

	return UnwrapImpl();
}

unittest {
	int a;
	string b;
	double[] c;
	unwrap(a, b, c) = from!"std.typecons".tuple(1, "b", [1.0, 4.3]);
	assert(a == 1);
	assert(b == "b");
	assert(c == [1.0, 4.3]);
}

/++
Template that generates sort of infix functions.
To use generated function, surround it with any of overloadable binary operators.

adr thinks this is disgusting.

Params:
	fun	= function that takes two arguments, lhs and rhs respectively

Examples:
---
enum w = Infix!((a, b) => a + b);
enum map = Infix!((a, b) => from!"std".map!(a => a.b)(a));
assert(2 ~w~ 3 == 5);
assert([1, 2, 3] <<map>> ((int a) => a * a));
---
+/
template Infix(alias fun) {
	struct Impl {
		auto opBinaryRight(string op, T)(T lhs) {
			struct Other {
				auto opBinary(string other, U)(U rhs) {
					return fun(lhs, rhs);
				}
			}
			return Other();
		}
	}
	enum Infix = Impl();
}

unittest {
	import std : array, stdMap = map;
	enum w = Infix!((a, b) => a + b);
	enum map = Infix!((a, b) => (a.stdMap!(a => b(a)).array));
	assert(2 ~w~ 3 == 5);
	assert([1, 2, 3] <<map>> ((int a) => a * a) == [1, 4, 9]);
}
