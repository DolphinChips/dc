module dc.dotbuilder;

///
unittest {
	assert(cast(string) dotBuilder.hi == "hi");
	assert(cast(string) dotBuilder.something.more.complex.and.longer == "something.more.complex.and.longer");
	static assert(cast(string) dotBuilder.compile.time.building.i.guess == "compile.time.building.i.guess");
}

/// `opDispatch`-based string builder.
enum dotBuilder = Impl!null();

private:
struct Impl(string built_) {
	enum built = built_;
	alias built this;

	enum opDispatch(string part) = Impl!(built == null ? part : built ~ '.' ~ part)();
}
