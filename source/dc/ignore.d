module dc.ignore;

unittest {
	static assert(__traits(compiles, {
		_ = 2 + 3;
		_ = _;
	}));
}

/++
`_` is a enum that provides overloaded `=` operator. That overload takes a value and promptly throws it away.
To avoid unnecessary copies, argument has `in` attribute. Don't know if it actually does any good though.
Examples:
---
_ = 2 + 3;
_ = new A();
---
+/
enum _ = Impl();

private:
struct Impl {
	/++
	Take an argument, throw it away and do nothing.
	Params:
		first = value to be ignored.
	+/
	pragma(inline, true);
	void opAssign(T)(in T) inout { }
}
