/++
This module adds declares a mixin template for automatically generating class constructor.
Generated constructor takes 1 argument per each field declared with `@wired` attribute. Later each argument gets assigned to its respective field.
Examples:
---
class A {
	@wired {
		int a;
		string b;
		float c;
	}

	mixin wire;
}
auto a = new A(1, "hi", 42.0);
assert(a.a == 1 && a.b == "hi" && a.c == 42.0);
---
+/
module dc.wire;

import dc : from;
import dc.dotbuilder : d = dotBuilder;

unittest {
	static assert(!__traits(compiles, {
		struct B {
			@wired ubyte a;

			mixin wire;
		}
	}));

	class A {
		@wired {
			int a;
			string b;
			float c;
		}

		mixin wire;
	}
	auto a = new A(1, "hi", 42.0);
	assert(a.a == 1 && a.b == "hi" && a.c == 42.0);
}
/// An attribute to decorate fields about which `wire` should care
enum wired;

/// Mixin template that generates our class constructor
mixin template wire() {
	static assert(is(typeof(this) == class), from!(d.std.traits).fullyQualifiedName!(typeof(this)) ~ " is not a class");
	private {
		template WiredFields(T) {
			import std.traits : hasUDA, FieldNameTuple;
			import std.meta : Filter;
			enum bool isWiredField(alias field) = hasUDA!(__traits(getMember, T, field), wired);
			alias WiredFields = Filter!(isWiredField, FieldNameTuple!T);
		}

		template WiredTypes(T) {
			import std.meta : staticMap;
			alias GetType(alias field) = typeof(__traits(getMember, T, field));
			alias WiredTypes = staticMap!(GetType, WiredFields!T);
		}
	}

	this(WiredTypes!(typeof(this)) args) {
		alias This = typeof(this);
		alias fields = WiredFields!This;
		static foreach(i, field; fields) {
			__traits(getMember, this, field) = args[i];
		}
	}
}
